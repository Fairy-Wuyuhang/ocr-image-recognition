package com.wyhsy.ocrtest.config;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.wyhsy.ocrtest.entity.OCRInfo;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class JsonToOcr {

    /**
     * 1.将json转为指定类型数据
     * @param jsonArray
     * @return
     */
    public String jsonToOcrInfos(JSONArray jsonArray){
        List<OCRInfo> list=new ArrayList<>();
        String result="";
        for (int i=0;i<jsonArray.size();i++){
            JSONArray area= jsonArray.getJSONArray(i);
            OCRInfo ocrInfo=new OCRInfo();
            ocrInfo.setArea(JSON.parseArray(area.getString(0),Double.class));
            ocrInfo.setText(area.getString(1));
            ocrInfo.setPercent(area.getDouble(2));
            String re = ocrInfo.getText();
            if (re.length()>0){
                result+=re;
            }
        }
        return result;
    }

}
