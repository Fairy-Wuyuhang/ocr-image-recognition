package com.wyhsy.ocrtest.config;

import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class MessageToData {

    /**
     * 1.参数
     */
    public String pattern;
    public Pattern r;
    public Matcher m;
    public Map<String, Object> res = new HashMap<>();

    /**
     * 2.将接口回调的数据转为指定格式
     *
     * @return
     */
    public Map<String, Object> CardToData(String message) {
        //1.学号
        pattern = "\\d+";
        r = Pattern.compile(pattern);
        m = r.matcher(message);
        String sno = m.find() ? m.group() : "";
        System.out.println("学号：" + sno);
        res.put("sno", sno);

        //2.学院
        pattern = "(院系:).*(学院)";
        r = Pattern.compile(pattern);
        m = r.matcher(message);
        String school = m.find() ? m.group() : "";
        System.out.println(school);
        res.put("school", school);

        //3.学生名字
        pattern = "(姓名:).*(帐号)";
        r = Pattern.compile(pattern);
        m = r.matcher(message);
        String sname = m.find() ? m.group() : "";
        sname = sname.replaceAll("帐号", "");
        System.out.println(sname);
        res.put("sname", sname);

        return res;
    }

}
