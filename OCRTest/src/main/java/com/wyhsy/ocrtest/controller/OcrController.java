package com.wyhsy.ocrtest.controller;

import com.wyhsy.ocrtest.config.JsonToOcr;
import com.wyhsy.ocrtest.config.MessageToData;
import com.wyhsy.ocrtest.entity.OCRInfo;
import com.wyhsy.ocrtest.entity.R;
import com.wyhsy.ocrtest.service.OcrService;
import io.swagger.annotations.Api;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.regex.Pattern;

@RestController
@Api(description = "疫情实时信息数据比对管理")
@RequestMapping("/ocrservice")
public class OcrController {

    @Autowired
    private OcrService ocrService;
    @Autowired
    private MessageToData messageToData;

    /**
     * 1.将指定文件转义为数据
     * @param file
     * @return
     */
    @PostMapping("traverse")
    @ResponseBody
    public R traverse(@RequestParam("file") MultipartFile file) throws IOException {
        File f = new File("C:\\Users\\Administrator\\Pictures\\Saved Pictures\\test.jpg");

        FileUtils.copyInputStreamToFile(file.getInputStream(), f);
        String ocrInfo = ocrService.getOcrInfo(OCRInfo.url, f);
        Map<String, Object> map = messageToData.CardToData(ocrInfo);
        return R.ok().data(map);
    }


}
