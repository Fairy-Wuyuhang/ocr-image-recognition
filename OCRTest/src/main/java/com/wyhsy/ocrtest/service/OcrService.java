package com.wyhsy.ocrtest.service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.wyhsy.ocrtest.config.JsonToOcr;
import com.wyhsy.ocrtest.entity.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.File;

@Service
public class OcrService {

    //1.请求的接口
    private static final String OcrUrl = "http://82.157.198.247:8089/api/tr-run/";

    @Autowired
    public RestTemplate restTemplate;
    @Resource
    public JsonToOcr jsonToOcr;

    /**
     * 2.请求识别接口得到数据
     *
     * @param url：请求的接口
     * @param image：图片资源
     * @return
     */
    public String getOcrInfo(String url, File image) {
        //1.设置请求头
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);

        //2.设置请求体
        FileSystemResource fileSystemResource = new FileSystemResource(image);
        MultiValueMap<String, Object> map = new LinkedMultiValueMap<>();
        map.add("file",fileSystemResource);

        //3.HttpEntity去封装请求报文
        HttpEntity<MultiValueMap<String, Object>> httpEntity = new HttpEntity<>(map, headers);

        //4.请求接口
        ResponseEntity<String> response = restTemplate.postForEntity(url, httpEntity, String.class);

        //5.将响应的数据进行转换json，并得到行数据
        JSONObject jsonObject = JSON.parseObject(response.getBody());
        if(jsonObject.getInteger("code")==200){
            String message = jsonToOcr.jsonToOcrInfos(jsonObject.getJSONObject("data").getJSONArray("raw_out"));
            return message;
        }
        return null;
    }


}
