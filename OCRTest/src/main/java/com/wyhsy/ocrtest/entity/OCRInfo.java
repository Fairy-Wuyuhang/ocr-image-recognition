package com.wyhsy.ocrtest.entity;

import io.swagger.annotations.ApiModel;

import java.util.List;

/**
 * @Description TODO
 * @Author crzep
 * @Date 2020/10/29 12:14
 * @VERSION 1.0
 **/
@ApiModel(value = "OCR对象",description = "OCR中间测试对象")
public class OCRInfo {

    public static final String url="http://82.157.198.247:8089/api/tr-run/";

    /**
     * 一共四个参数
     * 第1、2个：x起点 y起点像素值
     * 第3、4个：识别长、宽像素点
     * 第5个：识别矩形的旋转度数(顺时针)
     */
    private List<Double> area;
    /**
     * 识别结果
     */
    private String text;
    /**
     * 识别准确度百分比
     */
    private Double percent;

    public List<Double> getArea() {
        return area;
    }

    public void setArea(List<Double> area) {
        this.area = area;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Double getPercent() {
        return percent;
    }

    public void setPercent(Double percent) {
        this.percent = percent;
    }

//    @Override
//    public String toString() {
//        return "OCRInfo{" +
//                "area=" + area +
//                ", text='" + text + '\'' +
//                ", percent=" + percent +
//                '}';
//    }
    @Override
    public String toString() {

        return  text;
    }
}
