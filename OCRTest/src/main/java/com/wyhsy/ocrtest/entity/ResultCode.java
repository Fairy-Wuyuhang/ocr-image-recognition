package com.wyhsy.ocrtest.entity;

public interface ResultCode {
//定义成功失败的编码
    public static Integer SUCCESS = 20000;

    public static Integer ERROR = 20001;
}
